*** Settings ***
Library    org.robot.database.keywords.DatabaseLibrary
Resource    ../src/main/robot/database/adapter.robot

*** Variables ***
${ORACLE_DRIVER_CLASS}    oracle.jdbc.OracleDriver
${ORACLE_CONNECTION_STRING_PREFIX}    jdbc:oracle:thin:@//
${SYBASE_DRIVER_CLASS}    net.sourceforge.jtds.jdbc.Driver
${SYBASE_CONNECTION_STRING_PREFIX}    jdbc:jtds:sybase://
${MSSQL_DRIVER_CLASS}    com.microsoft.sqlserver.jdbc.SQLServerDriver
${MSSQL_CONNECTION_STRING_PREFIX}    jdbc:sqlserver://
#${MSSQL_DB_LOCAL_SERVER}    localhost\\SQL2012:1433;
${CSV_CONNECTION_STRING_PREFIX}    jdbc:jstels:csv:
${CSV_DRIVER_CLASS}    jstels.jdbc.csv.CsvDataSource2
${plugins}    org.h2.Driver