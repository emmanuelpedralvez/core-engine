*** Variables ***
${ALL_STRING_CHARACTERS_REGEX}    [a-zA-Z0-9_$+,:;=@#|'<>.^*()%!\\-"]+
${DATE_FORMAT_DD_MM_YYYY}    ^(?:(?:31(\\/)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\\1|(?:(?:29|30)(\\/)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/)(?:0?2|(?:Feb))\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$
${LINES}    ==============================================================================
${TEMP}    ${EMPTY}

*** Keywords ***
Evaluate math expression
    [Arguments]    ${expression}    ${field_format}=not_decimal    ${precision}=2
    ${value}      Evaluate     ${expression}
    ${value}    Run Keyword If    '${field_format}' == 'not_decimal'    Get Variable Value    ${value}
    ...    ELSE IF    '${field_format}' == 'decimal'    Convert To Number    ${value}    ${precision}
    ...    ELSE IF    '${field_format}' == 'trim'    Get Variable Value    ${value.strip()}0
    [Return]    ${value}

Load file content
    [Arguments]    ${file}
    ${file_content}    Get Binary File    ${TEST_DATA_DIR}/artefacts/${file}
    [Return]    ${file_content}

Format date
    [Arguments]    ${value}
    ${value}    Get Variable Value    ${value.strip()}
    ${value}    Replace String Using Regexp    ${value}    "$    ${EMPTY}
    ${value}    Replace String Using Regexp    ${value}    ^"    ${EMPTY}
    ${value}    Replace String Using Regexp    ${value}    \\sAM|\\sPM    ${EMPTY}
    ${value}    Replace String Using Regexp    ${value}    Z    ${EMPTY}
    ${value}    Replace String Using Regexp    ${value}    T    ${SPACE}
    ${value}    Replace String Using Regexp    ${value}    \\+\\d+    ${EMPTY}
    [Return]    ${value}

Strip quotes
    [Arguments]    ${value}
    ${value}    Get Variable Value    ${value.strip()}
    ${value}    Replace String Using Regexp    ${value}    "$    ${EMPTY}
    ${value}    Replace String Using Regexp    ${value}    ^"    ${EMPTY}
    [Return]    ${value}

Remove decimal places
    [Arguments]    ${value}
    ${value}    Replace String Using Regexp    ${value}    \\.\\d+    ${EMPTY}
    [Return]    ${value}

Reduce linebreak
    [Arguments]    ${value}
    ${value}    Replace String    ${value}    \n    ${EMPTY}
    [Return]    ${value}

Reduce whitespace
    [Arguments]    ${value}
    ${value}    Replace String Using Regexp    ${value}    ^ +| +$|( )+    ${SPACE}
    [Return]    ${value}