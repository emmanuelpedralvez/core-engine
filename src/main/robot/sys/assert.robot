*** Keywords ***
Data should match
    [Tags]    flatten
    [Arguments]    ${actual_value}    ${expected_value}    ${trim}=true
    ${actual_value}    Run Keyword If    '${trim}' == 'true'    Get Variable Value    ${actual_value.strip()}
    ...    ELSE    Set Variable    ${actual_value}
    ${expected_value}    Run Keyword If    '${trim}' == 'true'    Get Variable Value    ${expected_value.strip()}
    ...    ELSE    Set Variable    ${expected_value}
    Should Be Equal    ${actual_value}    ${expected_value}

Data should not match
    [Tags]    remove
    [Arguments]    ${actual_value}    ${expected_value}
    Should Not Be Equal    ${actual_value}    ${expected_value}

Data should contain
    [Tags]    remove
    [Arguments]    ${actual}    ${expected}
    Should Contain    ${actual}    ${expected}

Data should not contain
    [Tags]    remove
    [Arguments]    ${actual}    ${expected}
    Should Not Contain    ${actual}    ${expected}

Maps should be equal
    [Tags]    remove
    [Arguments]    ${actual}    ${expected}
    Log    ${actual}
    Log    ${expected}
    Dictionaries should be equal    ${actual}    ${expected}

Date should be equal
    [Tags]    remove
    [Arguments]    ${actual_value}    ${expected_value}    ${actual_value_date_format}=%d/%m/%Y    ${expected_value_date_format}=%m/%d/%Y
    ${actual_value}    Convert date    ${actual_value}    date_format=${actual_value_date_format}    result_format=%d/%m/%Y
    ${expected_value}    Convert date    ${expected_value}    date_format=${expected_value_date_format}    result_format=%d/%m/%Y
    Should Be Equal As Strings    ${actual_value}    ${expected_value}

Element Is Required
    [Arguments]     ${locator}
    ${attr}     Catenate    SEPARATOR=@     ${locator}      required
    ${attr}     Get Element Attribute       ${attr}
    Should Be Equal As Strings    ${attr}    true

Element Is Not Required
    [Arguments]    ${csslocator}
    ${attr}    Catenate    SEPARATOR=@    ${csslocator}    required
    ${attr}    Get Element Attribute    ${attr}
    Should Be Equal As Strings    ${attr}    None

Element Text Is Correct
    [Arguments]    ${locator}    ${text}    ${index}
    ${element_text}    Get Element Text    ${locator}    ${index}
    Should Be Equal As Strings    ${element_text}    ${text}

Element Is A Textbox
    [Arguments]    ${locator}
    ${attr}    Catenate    SEPARATOR=@    ${locator}    type
    ${attr}    Get Element Attribute    ${attr}
    Should Be Equal As Strings    ${attr}    text

Element Is A Checkbox
    [Arguments]    ${locator}
    ${attr}    Catenate    SEPARATOR=@    ${locator}    type
    ${attr}    Get Element Attribute    ${attr}
    Should Be Equal As Strings    ${attr}    checkbox

Find String
    [Tags]    remove
    [Arguments]    ${object}    ${arg}
    ${value}    Call Method    ${object}    find    ${arg}
    Run Keyword If    ${value}<=0    FAIL    Object Not Found: ${arg}