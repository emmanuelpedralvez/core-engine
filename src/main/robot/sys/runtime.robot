*** Keywords ***
Print
    [Tags]    flatten
    [Arguments]    ${message}
    Log    ${message}
    Log to console    \n${message}

Pause
    [Tags]    remove
    Pause Execution    Paused