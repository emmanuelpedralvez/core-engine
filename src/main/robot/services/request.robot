*** Settings ***
Resource    object.robot

*** Keywords ***
Instantiate service request
    Get base request    ${request}
    Create test request    ${request}

Get base request
    [Tags]    flatten
    [Arguments]    @{request_objects}
    :FOR    ${request}    IN    @{request_objects}
    \    ${json_count}    Get count    ${${request}.body_file.base}    .json
    \    ${request_base_body}    Run Keyword If    ${json_count} > 0
    \    ...    Get Binary File    ${PROJECT_TEST_RESOURCES}/json/${${request}.body_file.base}
    \    ...    ELSE    Set variable    ${${request}.body_file.base}
    \    Set Suite Variable    ${${request}.body.base}    ${request_base_body}
    \    Set Suite Variable    ${${request}.url.base}     ${${request}.url.base}

Create test request
    [Tags]    flatten
    [Arguments]    @{request_objects}
    :FOR    ${request}    IN    @{request_objects}
    \    Set Suite Variable    ${${request}.method}    ${${request}.method.base}
    \    Set Suite Variable    ${${request}.header}    ${${request}.header.base}
    \    Set Suite Variable    ${${request}.path}    ${${request}.path.base}
    \    Set Suite Variable    ${${request}.parameter}    ${${request}.parameter.base}
    \    Set Suite Variable    ${${request}.body}    ${${request}.body.base}
    \    Set Suite Variable    ${${request}.attachment}    ${${request}.attachment.base}
    \    Set Suite Variable    ${${request}.url}    ${${request}.url.base}

Set request method
    [Tags]    flatten
    [Arguments]    ${method}=${EMPTY}
    Set Suite Variable    ${${request}.method}    ${method}

Set request url
    [Arguments]    ${url}=${EMPTY}
    [Tags]    flatten
    Set Suite Variable    ${${request}.url}    ${url}

Set request body
    [Tags]    flatten
    [Arguments]    &{request_data}
    ${request_body}      Get Variable Value     ${${request}.body}
    ${items}    Get Dictionary Items    ${request_data}
    :FOR    ${key}    ${value}    IN    @{items}
    \    ${request_body}    Set Json Value    ${request_body}    ${key}    ${value}
    Set Suite Variable    ${${request}.body}    ${request_body}

Set request body from file
    [Tags]    flatten
    [Arguments]    ${file}
    ${request_body}    Get Binary File    ${PROJECT_TEST_RESOURCES}/json/${file}
    Set Suite Variable    ${${request}.body}    ${request_body}

Set request header
    [Tags]    flatten
    [Arguments]    &{request_data}
    Set To Dictionary    ${${request}.header}    &{request_data}

Set request path
    [Arguments]    ${path}
    [Tags]    flatten
    Set Suite Variable    ${${request}.path}    ${path}

Set request parameter
    [Tags]    flatten
    [Arguments]    &{dict}
    ${items}    Get Dictionary Items    ${dict}
    :FOR    ${key}    ${value}    IN    @{items}
    \    ${TEMP}    Set Variable    ${TEMP}${key}=${value}&
    Set Suite Variable    ${${request}.parameter}    ${TEMP}

Change request path
    [Tags]    flatten
    [Arguments]    &{request_data}
    ${request_path}      Get Variable Value     ${${request}.path}
    ${items}    Get Dictionary Items    ${request_data}
    :FOR    ${key}    ${value}    IN    @{items}
    \    ${request_path}    Replace String Using Regexp    ${request_path}    (?<=${key}=)${ALL_STRING_CHARACTERS_REGEX}    ${value}
    Set Suite Variable    ${${request}.path}    ${request_path}

Change request parameter
    [Tags]    flatten
    [Arguments]    &{request_data}
    ${request_parameter}      Get Variable Value     ${${request}.parameter}
    ${items}    Get Dictionary Items    ${request_data}
    :FOR    ${key}    ${value}    IN    @{items}
    \    ${value}    Convert to string    ${value}
    \    ${request_parameter}    Replace String Using Regexp    ${request_parameter}    (?<=\\b${key}=)(${ALL_STRING_CHARACTERS_REGEX}|$)(?![&?]$)    ${value}
    Set Suite Variable    ${${request}.parameter}    ${request_parameter}

Send request
    [Tags]     flatten
    Create Session    alias=${request}    url=${${request}.url}    headers=${${request}.header}
    ${status}    ${response}
    ...    Run Keyword If    '${${request}.method}'=='Post'    Run Keyword And Ignore Error
    ...    Post Request    ${request}    ${${request}.path}?${${request}.parameter}     ${${request}.body}
    ...    ELSE IF    '${${request}.method}'=='Get'    Run Keyword And Ignore Error
    ...    Get Request    ${request}    ${${request}.path}    params=${${request}.parameter}
    ...    ELSE IF    '${${request}.method}'=='Put'    Run Keyword And Ignore Error
    ...    Put Request    ${request}    ${${request}.path}?${${request}.parameter}    ${${request}.body}
    ...    ELSE IF    '${${request}.method}'=='Delete'    Run Keyword And Ignore Error
    ...    Delete Request    ${request}    ${${request}.path}?${${request}.parameter}    ${${request}.body}
    Set Suite Variable    ${response}
    Print    ${response.content}

Establish request session
    Create Session    alias=${request}    url=${${request}.url}    headers=${${request}.header}

Close all request session
    Delete All Sessions