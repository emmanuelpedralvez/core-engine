*** Settings ***
Resource    object.robot

*** Keywords ***
Get json array element
    [Documentation]    Returns count of items inside a json array
    [Arguments]    ${json}    ${json_path}
    ${json}    Run Keyword If    '${json_path}' == '${EMPTY}'    Get Variable Value    ${json}
        ...    ELSE    Get Json Value    ${json}    ${json_path}
    ${json_dictionary}    Parse Json    ${json}
    [Return]    ${json_dictionary}

Get json array
    [Documentation]    Returns items inside a json array
    [Arguments]    ${json}    ${json_path}
    ${json}    Run Keyword If    '${json_path}' == '${EMPTY}'    Get Variable Value    ${json}
        ...    ELSE    Get Json Value    ${json}    ${json_path}
    ${json_dictionary}    Parse Json    ${json}
    [Return]    ${json_dictionary}

Get json array element count
    [Documentation]    Returns count of items inside a json array
    [Arguments]    ${json}    ${json_path}
    ${json}    Run Keyword If    '${json_path}' == '${EMPTY}'    Get Variable Value    ${json}
        ...    ELSE    Get Json Value    ${json}    ${json_path}
    ${json_dictionary}    Parse Json    ${json}
    ${count}    Get Length    ${json_dictionary}
    [Return]    ${count}

Get text count
    [Documentation]    Returns count of given text inside a json slice
    [Arguments]    ${json}    ${json_path}    ${expected_text}
    ${json}    Run Keyword If    '${json_path}' == '${EMPTY}'    Get Variable Value    ${json}
    ...    ELSE    Get Json Value    ${json}    ${json_path}
    ${count}    Get Count    ${json}    ${expected_text}
    [Return]    ${count}

Get json field value
    [Arguments]    ${json}    ${field}    ${field_format}=not_decimal    ${number_precision}=2    ${date_format}=%d/%m/%Y    ${result_format}=%d/%m/%Y
    ${value}    Get Json Value    ${json}    ${field}
    ${value}    Replace String    ${value}    "    ${EMPTY}
    ${value}    Run Keyword If    '${field_format}' == 'not_decimal'    Get Variable Value    ${value}
    ...    ELSE IF    '${field_format}' == 'decimal'    Convert To Number    ${value}    ${number_precision}
    ...    ELSE IF    '${field_format}' == 'date'    Convert Date    ${value}    date_format=${date_format}    result_format=${result_format}
    ...    ELSE IF    '${field_format}' == 'trim'    Get Variable Value    ${value.strip()}
    ...    ELSE IF    '${field_format}' == 'unquote'    Replace String    ${value}    "    ${EMPTY}
    [Return]    ${value}

Set json body
    [Arguments]    ${json}    &{request_data}
    ${count}    Get Count    ${json}    .txt
    ${json_body}    Run Keyword If    ${count} > 0    Get File    ${json}
    ...    ELSE    Get Variable Value    ${json}
    ${items}    Get Dictionary Items    ${request_data}
    :FOR    ${key}    ${value}    IN    @{items}
    \    ${json_body}    Set Json Value    ${json_body}    ${key}    ${value}
    [Return]    ${json_body}

Get json list
    [Arguments]    ${json}=${EMPTY}    ${json_path}=${EMPTY}
    @{json_path_elements}    Get Regexp Matches    ${json_path}    (^\\$\\.)    1
    ${json_path_count}    Get Length    ${json_path_elements}
    ${json_list}    Run Keyword If    ${json_path_count} > 0    Get json array by json path    ${json}    ${json_path}
    ...    ELSE    Get json array    ${json}    ${json_path}
    [Return]    ${json_list}

Get json array by json path
    [Arguments]    ${json}    ${json_path_query}
    ${json}    Find Json Element List    ${json}    ${json_path_query}
    ${json}    Convert To String    ${json}
    ${json}    To Json    ${json}   true
    ${json}    Parse Json    ${json}
   [Return]    ${json}

Get json list index of field
    [Arguments]    ${field}    ${expected_field_value}    ${json_path}=${EMPTY}
    ${json_list}    Get json list    ${response.text}    ${json_path}
    ${json_list_count}    Get Length    ${json_list}
    :FOR    ${index}    IN RANGE    ${json_list_count}
    \    ${json}    Stringify Json    @{json_list}[${index}]
    \    ${actual_field_value}    Get Json Value    ${json}    /${field}
    \    ${index_at}    Run Keyword If    '${actual_field_value}' == '${expected_field_value}'    Run Keywords    Get Variable Value    ${index}    Exit For Loop
    \    ...    ELSE    Get Variable Value    ${index}
    [Return]    ${index_at}