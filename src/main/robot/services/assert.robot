*** Settings ***
Resource    object.robot

*** Keywords ***
Response status code should be
    [Tags]    flatten
    [Arguments]    ${expected_response_code}
    Should Be Equal As Strings    ${response.status_code}    ${expected_response_code}

Response status code should not be
    [Tags]    flatten
    [Arguments]    ${expected_response_code}
    Should Not Be Equal As Strings    ${response.status_code}    ${expected_response_code}

Response body should be
    [Tags]    flatten
    [Arguments]    ${expected_response_text}
    ${actual_response_text}    To Json    ${response.text}
    ${expected_response_text}    To Json    ${expected_response_text}
    Should Be Equal    ${actual_response_text}    ${expected_response_text}

Response field should be
    [Tags]    flatten
    [Arguments]    ${xpath}    ${expected_response_field}
    ${actual_response_field}    Get json field value    ${response.text}    ${xpath}    trim
    ${actual_response_field}    Reduce whitespace    ${actual_response_field}
    Data should match    ${actual_response_field}    ${expected_response_field}

Response body should have
    [Tags]    flatten
    [Arguments]    ${expected_response_text}
    Should Contain    ${response.text}    ${expected_response_text}