*** Keywords ***
Database field value should be null
    [Arguments]    ${table}    ${column}    ${where_clause}
    ${actual_result}    Get database field value    ${table}    ${column}    ${where_clause}
    Should Be Equal    ${actual_result}    ${NONE}

Database field value should be
    [Arguments]    ${result}    ${table}    ${column}    ${where_clause}    ${data_type}=not_decimal
    ${value}    Get database field value    ${table}    ${column}    ${where_clause}    ${data_type}
    Should Be Equal As Strings    ${result}    ${value}

Database field value should not be
    [Arguments]    ${result}    ${table}    ${column}    ${where_clause}    ${data_type}=not_decimal
    ${value}    Get database field value    ${table}    ${column}    ${where_clause}    ${data_type}
    Should Not Be Equal As Strings    ${result}    ${value}

Database field should be
    [Arguments]    ${select_sql}    ${expected_value}    ${field}    ${db_alias}=${default_db}
    &{table}    Get record from table    ${select_sql}    ${db_alias}
    Should Be Equal As Strings    &{table}[${field}]    ${expected_value}

Database should not have
    [Arguments]    ${table}    ${where_clause}
    Row Should Not Exist In Table    ${table}    ${where_clause}

Database should have
    [Arguments]    ${table}    ${where_clause}
    ${status}    Run Keyword And Ignore Error    Run Keyword And Return    Row Should Not Exist In Table    ${table}    ${where_clause}
    ${status}    Convert To String    ${status}
    Should contain    ${status}    Row exists