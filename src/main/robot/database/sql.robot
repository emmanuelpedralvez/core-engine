*** Variables ***
${SQL_DIR}    ${PROJECT_TEST_RESOURCES}/sql

*** Keywords ***
Get record from table
    [Tags]    flatten
    [Documentation]    a simple keyword to get data record from database
    [Arguments]    ${select_sql}    ${database}=${DB_ALIAS}
    &{record}    Read Record From Table    ${database}    ${select_sql}
    Log    ${LINES}
    Log Dictionary    ${record}
    Log    ${LINES}
    Set Suite Variable    ${record}    ${record}
    [Return]    ${record}

Get value from record
    [Tags]    flatten
    [Arguments]    ${column}
    ${value}    Set Variable    &{record}[${column}]
    ${value}    Reduce whitespace    ${value}
    Set Suite Variable    ${value}    ${value}
    [Return]    ${value}

Get record count of table
    [Tags]    flatten
	[Documentation]    complex explanation
    [Arguments]    ${table}    ${where_clause}=${EMPTY}    ${database}=${default_db}
    &{records}    Get record from table    SELECT COUNT(*) AS 'count' FROM ${table} ${where_clause}    ${database}
    [Return]    &{records}[count]

Get database field value
    [Tags]    flatten
    [Documentation]    a simple keyword to get single value from database
    [Arguments]    ${table}    ${field}    ${where_clause}    ${field_type}=not_decimal
    ${value}    Read Single Value From Table    ${table}    ${field}    ${where_clause}
    ${value}    Run Keyword If    '${field_type}' == 'not_decimal'    Get Variable Value    ${value}
    ...    ELSE IF    '${field_type}' == 'decimal'    Convert To Number    ${value}    2
    ...    ELSE IF    '${field_type}' == 'date'    Convert Date    ${value}    result_format=%m/%d/%Y
    ...    ELSE IF    '${field_type}' == 'trim'    Get Variable Value    ${value.strip()}
    [Return]    ${value}

Update database record
    [Arguments]    ${table}    ${set_value}    ${where_clause}
    Execute Sql    UPDATE ${table} SET ${set_value} WHERE ${where_clause}

Restore database
    Execute SQL Script    ${DB_ALIAS}    USE [master]
    Execute SQL Script    ${DB_ALIAS}    ALTER DATABASE [${DB_ALIAS}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
    Execute SQL Script    ${DB_ALIAS}    RESTORE DATABASE [${DB_ALIAS}] FROM DISK = '${PROJECT_TEST_RESOURCES}backup\\${DB_ALIAS}-backup.bak' WITH FILE = 1, KEEP_REPLICATION, NOUNLOAD, REPLACE, STATS = 5
    Execute SQL Script    ${DB_ALIAS}    ALTER DATABASE [${DB_ALIAS}] SET MULTI_USER

Delete database record
    [Arguments]    ${table}    ${where_clause}
    Execute Sql    DELETE FROM ${table} WHERE ${where_clause}

Inject SQL script
    [Tags]    flatten
    [Arguments]    ${target_alias}    ${sql_script}
    Execute Sql    ${target_alias}    ${sql_script}

Execute SQL script
    [Tags]    flatten
    [Arguments]    ${sql}
    &{record}    Get record from table    ${sql}

Execute SQL from file
    [Tags]    flatten
    [Arguments]    ${file}    &{dict}
    Log    ${LINES}
    ${sql}    OperatingSystem.Get File    ${SQL_DIR}/${file}
    ${items}    Get Dictionary Items    ${dict}
    :FOR    ${key}    ${value}    IN    @{items}
    \    ${sql}    Replace String    ${sql}    {${key}}    ${value}
    &{record}    Get record from table    ${sql}

Insert test data from csv to database
    [Arguments]    ${csv_source}    ${source_alias}    ${target_alias}    ${target_table}
    Execute SQL    ${source_alias}    CREATE TABLE csv_copy AS SELECT * FROM CSVREAD('${csv_source}')
    Execute SQL    ${source_alias}    CREATE TABLE test_data_temp AS SELECT * FROM csv_copy; ALTER TABLE test_data_temp DROP COLUMN record_id;
    &{table_columns}    Read record from table    ${source_alias}    SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name='test_data_temp'
    @{table_columns}    Get Dictionary Values    ${table_columns}
    ${table_columns}    Catenate    SEPARATOR=||','||    @{table_columns}
    &{insert_into_values}    Read record from table    ${source_alias}    SELECT ${table_columns} FROM test_data_temp
    @{insert_into_values}    Get Dictionary Values    ${insert_into_values}
    ${sql_insert}    Set Variable    ${EMPTY}
    :FOR    ${values}    IN    @{insert_into_values}
    \    ${sql_insert}    Catenate    SEPARATOR=,    ${sql_insert}    VALUES(${values})
    ${sql_insert}    Replace String    ${sql_insert}    ,    ${EMPTY}    1
    ${sql_insert}    Catenate    INSERT INTO ${target_table}    ${sql_insert}
    Execute SQL    ${target_alias}    ${sql_insert}

Retrieve test data from csv
    [Arguments]    ${csv_source}    ${source_alias}    ${table_key}
    Execute SQL    ${source_alias}    CREATE TABLE csv_copy AS SELECT * FROM CSVREAD('${csv_source}')
    &{test_data_temp}    Read record from table    ${source_alias}    SELECT record_id||'==='||${table_key} as test_data FROM csv_copy
    Execute SQL    ${source_alias}    DROP TABLE csv_copy
    &{test_data}    Create Dictionary    empty=empty
    @{test_data_temp}    Get Dictionary Values    ${test_data_temp}
    :FOR    ${items}    IN    @{test_data_temp}
    \    @{item}    Split String    ${items}    ===
    \    ${test_data_value}    Replace String    @{item}[1]    '    ${EMPTY}
    \    Set To Dictionary    ${test_data}    @{item}[0]=${test_data_value}
    [Return]    ${test_data}
