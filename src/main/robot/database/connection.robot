*** Keywords ***
Establish ORACLE database connection
    [Arguments]    ${alias}
    Create Connection To Database    ${alias}    ${ORACLE_DRIVER_CLASS}
    ...    ${ORACLE_CONNECTION_STRING_PREFIX}${ORACLE_DB_SERVER}/${ORACLE_DB_NAME}    ${ORACLE_DB_USER}    ${ORACLE_DB_PASS}

Establish SYBASE database connection
    [Arguments]    ${alias}
    Create Connection To Database    ${alias}    ${SYBASE_DRIVER_CLASS}
    ...    ${SYBASE_CONNECTION_STRING_PREFIX}${SYBASE_DB_SERVER}/${SYBASE_DB_NAME}    ${SYBASE_DB_USER}    ${SYBASE_DB_PASS}

Establish MSSQL database connection
    [Arguments]    ${alias}    ${MSSQL_CONFIG}
    Create Connection To Database    ${alias}    ${MSSQL_DRIVER_CLASS}
    ...    ${MSSQL_CONNECTION_STRING_PREFIX}${MSSQL_CONFIG}    ${MSSQL_DB_USER}    ${MSSQL_DB_PASS}

Establish H2 database connection
    [Arguments]    ${alias}
    Create Connection To Database    ${alias}    ${H2_DRIVER_CLASS}
    ...    jdbc:h2:mem:${alias};DATABASE_TO_UPPER=false    ${EMPTY}    ${EMPTY}

Establish EXCEL database connection
    [Arguments]    ${alias}    ${file_path}
    Create Connection To Database    ${alias}    ${ODBC_DRIVER_CLASS}
    ...    ${ODBC_CONNECTION_STRING_PREFIX}Driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};DBQ=${file_path};READONLY=false;    ${EMPTY}    ${EMPTY}

Close all database connection
    Disconnect All Database

Set default database
    [Arguments]    ${alias}
    Set Database    ${alias}
    Set Suite Variable    ${default_db}    ${alias}