*** Keywords ***
Stop IIS
    OperatingSystem.Run    iisreset/stop

Start IIS
    OperatingSystem.Run    iisreset/start

Restart IIS
    OperatingSystem.Run    iisreset