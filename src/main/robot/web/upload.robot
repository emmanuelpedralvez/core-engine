*** Keywords ***
Upload
    [Arguments]    ${path}
    AutoItLibrary.Sleep    1000
    Send    ${path}
    Control Click    Open    ${EMPTY}    Button1

Upload On Button Click
    [Arguments]    ${locator}    ${path}
    Choose File    ${locator}    ${path}