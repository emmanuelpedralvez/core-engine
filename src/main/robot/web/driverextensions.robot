*** Keywords ***
Get Attribute Class
    [Arguments]    ${locator}
    ${attr}    Catenate    SEPARATOR=@    ${locator}    class
    ${attr}    Get Element Attribute    ${attr}
    [Return]    ${attr}

Get Attribute Title
    [Arguments]    ${locator}
    ${attr}    Catenate    SEPARATOR=@    ${locator}    title
    ${attr}    Get Element Attribute    ${attr}
    [Return]    ${attr}

Get Value From Multiple Id
    [Tags]    remove
    [Arguments]    ${id}
    ${id}    Replace String    ${id}    ${SPACE}    ${EMPTY}
    @{id}    Split String    ${id}    ,
    ${value}    Selenium2Library.Get Text    xpath=//*[contains(@id, '@{id}[0]')and contains(@id, '@{id}[1]')]
    [Return]    ${value}

Click
    [Tags]    remove
    [Arguments]    ${locator}
    Wait Until Element Is Clickable    ${locator}    ${WAIT_TIME}
    Click Element    ${locator}

Clear
    [Tags]    remove
    [Arguments]    ${locator}
    Wait Until Element Is Visible    ${locator}    ${WAIT_TIME}
    Clear Element Text    ${locator}

Send Keys
    [Tags]    remove
    [Arguments]    ${locator}    ${text}
    Wait Until Element Is Visible    ${locator}    ${WAIT_TIME}
    Press Key    ${locator}    ${text}

Get Data In Table
    [Arguments]    ${table_locator}    ${row}    ${column}
    Get Table Cell    ${table_locator}    ${row}    ${column}

Get Data Row Index From Table
    [Arguments]    ${table_row_xpath}    ${table_locator}    ${column_index}    ${data_name}
    Wait Until Element Is Visible    ${table_row_xpath}
    ${row_count}    Get Matching Xpath Count    ${table_row_xpath}
    @{list}    Create List
    :FOR    ${INDEX}    IN RANGE    1     ${row_count} + 1
    \    ${data}    Get Table Cell    ${table_locator}    ${INDEX}    ${column_index}
    \    Append To List    ${list}    ${data}
    ${var}    Get Index From List    ${list}    ${data_name}
    Log To Console    ${var}
    [Return]    ${var}

Get Element Text
    [Arguments]    ${locator}     ${index}
    ${text}    Selenium2Library.Get Text    ${locator}
    ${final}    Get Line    ${text}    ${index}
    [Return]     ${final}

Get Text Field Value By Id
    [Arguments]    ${locator_id}
    ${value}    Execute Javascript    return document.getElementById('${locator_id}').value;
    [Return]    ${value}

Scroll
    [Tags]    remove
    [Arguments]    ${x}    ${y}
    Execute Javascript    window.scrollBy(${x},${y})

Scroll To Bottom
    Execute Javascript    window.scrollTo(0,document.body.scrollHeight);


Sleep
    [Arguments]    ${sec}
    ${time}    Evaluate math expression    ${sec} * 1000
    AutoItLibrary.Sleep    ${time}


