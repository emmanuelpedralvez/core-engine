*** Keywords ***
Element Is Displayed
    [Arguments]     ${locator}
    Wait Until Element Is Visible    ${locator}    ${WAIT_TIME}
    Element Should Be Visible    ${locator}    ${ELEMENT_IS_NOT_VISIBLE}

Element Is Not Displayed
    [Arguments]    ${locator}
    Element Should Not Be Visible    ${locator}

Element Text Is Displayed As
    [Arguments]    ${locator}    ${text}
    Wait Until Element Is Visible    ${locator}    ${WAIT_TIME}
    Element Text Should Be    ${locator}    ${text}    ${TEXT_IS_NOT_DISPLAYED_AS_INDICATED}

Attribute Should Contain Value
    [Arguments]    ${locator}    ${value}
    ${attr}    Get Attribute Class    ${locator}
    Should Contain    ${attr}    ${value}

Attribute Should Not Contain Value
    [Arguments]    ${locator}    ${value}
    ${attr}    Get Attribute Class    ${locator}
    Should Not Contain    ${attr}    ${value}

Element Is Enabled
    [Arguments]    ${locator}    ${is_button}=yes
    Run Keyword If    '${is_button}' == 'no'
    ...    Attribute Should Not Contain Value    ${locator}    disabled
    ...    ELSE IF    '${is_button}' == 'yes'
    ...    Element Should Be Enabled    ${locator}

Element Is Disabled
    [Arguments]    ${locator}    ${isbutton}=yes
    Run Keyword If    '${isbutton}'== 'no'
    ...    Attribute Should Contain Value    ${locator}    disabled
    ...    ELSE IF    '${isbutton}' == 'yes'
    ...    Element Should Be Disabled    ${locator}

Textbox Value Is Correct
    [Arguments]    ${locator_id}    ${expected_value}
    ${text_value}    Get Text Field Value By Id    ${locator_id}
    Data should match    ${text_value}    ${expected_value}

URL should be
    [Arguments]    ${URL}
    Location Should Be    ${URL}