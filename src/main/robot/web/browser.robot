
*** Keywords ***
Create Chrome Browser
    [Arguments]    ${URL}
    Set Webdriver Property    webdriver.chrome.driver    ${CORE_WEBDRIVER}/chromedriver.exe
    Open Browser Window    ${URL}    chrome

Create Internet Explorer Browser
    [Arguments]    ${URL}
    Set Webdriver Property    webdriver.ie.driver    ${CORE_WEBDRIVER}/IEDriverServer.exe
    Open Browser Window    ${URL}    ie

Create Firefox Browser
    [Arguments]    ${URL}
    Set Webdriver Property    webdriver.gecko.driver    ${CORE_WEBDRIVER}/geckodriver.exe
    Open Browser Window    ${URL}    firefox

Create Headless Browser
    [Arguments]    ${URL}
    Set Webdriver Property    phantomjs.binary.path    ${CORE_WEBDRIVER}/ghostdriver.exe
    Open Browser Window    ${URL}    phantomjs

Open Browser Window
    [Arguments]    ${URL}    ${browser}=chrome
    ${capabilities}    Set Variable     Set Webdriver Capability
    Open Browser    ${URL}    ${browser}    alias=NONE  remoteUrl=False
    Maximize Browser Window
    Log Title

Close Browser Window
    Close Window
