package com.tinkr;

import java.io.File;

public class Rearm {
    private static final String REPORT_BG = "white:white";
    private static final String LOG_MODE = "ansi";

    protected final void setLibraryCache() throws NoSuchFieldException, IllegalAccessException {
        Reload util = new Reload();
        util.setJavaLibrary(getDllSource().toString(), "jacob-1.18");
    }

    protected final File getDllSource() {
        return new File(this.getCoreMainResources(), "sys/dll");
    }

    protected final File getMainResources() {
        return new File("src/main/resources");
    }

    protected final File getTestResources() {
        return new File(getMainResources().toString().replaceAll("main", "test"));
    }

    protected final File getProjectRoot() {
        return new File(System.getProperty("user.dir"));
    }

    protected final File getProjectMainResources() {
        return new File(getProjectRoot(), getMainResources().toString());
    }

    protected final File getProjectTestResources() {
        return new File(getProjectRoot(), getTestResources().toString());
    }

    protected final File getCoreRoot() {
        return new File(getProjectRoot().toString().replaceAll(getProjectRoot().getName(), "core-engine"));
    }

    protected final File getCoreMainResources() {
        return new File(getCoreRoot(), getMainResources().toString());
    }

    protected final File getCoreWebdriver() {
        return new File(getCoreMainResources(), "web/driver");
    }

    protected final File getPythonPath() {
        return new File(getCoreRoot(), getMainResources().toString().replace("resources", "py"));
    }

    protected final String getReportBg() {
        return this.REPORT_BG;
    }

    protected final String getLogMode() {
        return this.LOG_MODE;
    }
}