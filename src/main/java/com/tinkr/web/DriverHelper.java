package com.tinkr.web;

import org.json.JSONObject;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.remote.DesiredCapabilities;

public class DriverHelper {
    public static final DriverHelper Default = new DriverHelper();

    public String setWebDriverProperty(String binary, String dir) {
        return System.setProperty(binary, dir);
    }

    public DesiredCapabilities setWebDriverCapability() {
        Proxy proxy = new Proxy();
        proxy.setHttpProxy("52.77.189.114:18888");
        proxy.setSocksUsername("emmanuel.pedralvez");
        proxy.setSocksPassword("Pha2ucEv");

        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability("proxy",proxy);

        return desiredCapabilities;

    }
}