package com.tinkr;

import java.lang.reflect.Field;

public class Reload {
    protected void setJavaLibrary(String path, String name) throws NoSuchFieldException, IllegalAccessException {
        System.setProperty("java.library.path", path);
        Class clazz = ClassLoader.class;
        Field userPaths = null;
        userPaths = clazz.getDeclaredField("sys_paths");
        userPaths.setAccessible(true);
        userPaths.set(null, null);
        System.loadLibrary(name);
    }
}